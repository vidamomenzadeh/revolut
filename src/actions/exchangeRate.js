import {GET_FX_URL} from '../constants/config';

let fetchQuestions = () => {
    return fetch(GET_FX_URL, {
        method: 'GET'      
    }).then(response => response.json())
}

export const geRatesData = () => dispatch => {
    return fetchQuestions().then((res) => {
        dispatch({
            type: "FETCHED_RATES",
            payload: {
                rates : res.rates
            }
        });
    });
}
