import { combineReducers } from 'redux';

let initialState = {
    listRates  : []
}


export const rates = (state = initialState.listRates, action)=> {
    switch (action.type) {
        case "FETCHED_RATES":
        
            return{
                ...state,
                ...action.payload.rates
            }

        default:
            return state;
    }
};

export const getRates = (state) => {
    return  state ? state.exchangeRate.rates : [];
};


export default combineReducers({
    rates
});
