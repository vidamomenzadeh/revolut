import { combineReducers } from 'redux';
import exchangeRate from './exchangeRate';


export const reducers = combineReducers({
    exchangeRate
});
