import React from "react";
import { shallow, mount } from 'enzyme';
import ExchangePageContainer from '../containers/ExchangePageContainer';
import ExchangeForm from '../components/exchange/exchangeForm';
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
 

describe('Exchange Container', () => {
  it('renders correctly', () => {
   const wrapper = shallow(<ExchangePageContainer />);
   expect(wrapper).toMatchSnapshot();
    // On the first run of this test, Jest will generate a snapshot file automatically.
  });
});


describe('Currency Rendered Correctly', () => {
    it('the app should have currency select', () => {
        const wrapper  = shallow(<ExchangePageContainer />);
        expect(wrapper.find('.selectcurrency')).toBeDefined();             
    })
})

describe('Calculate currency correctly', () => {
    it('calculate correctly', () => {
    	const rates={"GBP" : 0.77772, "EUR" : 0.87813, "USD": 1}
        const wrapper  = mount(<ExchangeForm rates={rates}/>);

        const haveCurrencyInput = wrapper.find("input.haveCurrency")
  		haveCurrencyInput.instance().value = 100
  		haveCurrencyInput.simulate('change');
  		expect(wrapper.find("input.haveCurrency").instance().value + "").toBe("100");
  		expect(wrapper.state().currencyWantAmount).toBe("88.57");		
    })
})