import React from "react";
import { Component } from 'react';
import { connect } from 'react-redux';
import ExchangeForm from '../components/exchange/exchangeForm';
import {geRatesData} from '../actions/exchangeRate';

import { getRates }  from '../reducers/exchangeRate';

import {withRouter} from "react-router";

import LayoutCmp from '../components/LayoutCmp';


class ExchangePageContainer extends Component {

    constructor(props) {
        super(props);        
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

    componentDidMount() {
        var $this = this;
        this.props.geRatesData();
        this.interval = setInterval(() => $this.props.geRatesData(), 10000);
    }
 
    render() {
        const {rates} = this.props;        

        return(
            <LayoutCmp  showFooter={true} 
                        showHeader={true}>                
                <div className="App">                                      
                    <ExchangeForm  rates={rates} />                        
                </div>
            </LayoutCmp>
        )
    }
}

function mapStateToProps(state) {
    return {
        rates : getRates(state)
    }
}

export default withRouter(connect(mapStateToProps, {
    geRatesData : geRatesData    
})(ExchangePageContainer));
