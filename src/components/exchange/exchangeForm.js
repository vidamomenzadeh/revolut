import React, { Component } from 'react';


export default class ExhangeForm extends Component{

    constructor(props) {
        super(props);

        this.currencies = ["GBP", "EUR", "USD"];
        this.signs = {"GBP" : "£", "EUR": "€", "USD" : "$"};

        this.state = {
            currencyWant : "GBP",
            currencyHave : "EUR",            
            currencyWantAmount : 0,
            currencyHaveAmount : 0
        }       
    }  

    handelCurrencyChange(event){
      if(event.target.name == 'currencyhave'){
          this.setState({
            currencyHave : event.target.value
          }, () => {
              this.handleInputChange();
          })
      }else{
        this.setState({
          currencyWant: event.target.value
        }, () => {
              this.handleInputChange();
        })
      }

      
    }  

    handleInputChange(){
      const currencyIHaveAmount = this.refs.currencyamount.value,
      //Users just have to type numbers
      re = /^[0-9\b]+$/,
      { rates } = this.props;    
      let currencyWantAmount;

      if (!(currencyIHaveAmount === '' || re.test(currencyIHaveAmount))) {
        return;
      }

      currencyWantAmount = parseFloat((currencyIHaveAmount * (rates[this.state.currencyWant] / rates[this.state.currencyHave])).toFixed(2));

      this.setState({
        currencyHaveAmount: currencyIHaveAmount,
        currencyWantAmount : currencyWantAmount
      });   
    }
    
    render(){
        const { rates } = this.props;        
        let $this = this,
        currencyWantAmount = parseFloat(((rates[this.state.currencyWant] / rates[this.state.currencyHave])).toFixed(2));

        return(
           <div className="currencywrapper">
                <div className="currencyhave">    
                  <div className="showcurrenctcurrency">{this.signs[$this.state.currencyHave]} 1 = {this.signs[$this.state.currencyWant]} {currencyWantAmount}</div>              
                  <select name="currencyhave" className="selectcurrency" onChange={this.handelCurrencyChange.bind(this)}>   
                    {this.currencies.map(function(name, index){
                      return <option value={name} selected={name == $this.state.currencyHave}>{name}</option>;
                    })}
                  </select>                                   
                  <input className="haveCurrency" ref="currencyamount" type="text" value={this.state.currencyHaveAmount} onChange={this.handleInputChange.bind(this)}/>                 
                </div>
                <div className="arrow-down"></div>
                <div className="currencywant"> 
                  <select  name="currencywanted" className="selectcurrency" onChange={this.handelCurrencyChange.bind(this)}>   
                    {this.currencies.map(function(name, index){
                      return <option value={name} selected={name == $this.state.currencyWant}>{name}</option>;
                    })}
                  </select>                                   
                   <input disabled className="wantedCurrency" type="text" value={this.state.currencyWantAmount}/>                   
                </div>
            </div>
        );
    }
}
