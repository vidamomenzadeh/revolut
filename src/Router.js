import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import { Router, IndexRoute, useRouterHistory } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import ExchangePageContainer from './containers/ExchangePageContainer';



const Root = ({store, history}) => {
    return <Provider store={store}>
        <Router history={history}>
            <Fragment>
                <Switch>
                    <Route exact path='/' component={ExchangePageContainer} />                   
                </Switch>
            </Fragment>
        </Router>
    </Provider>
};

export default Root;
